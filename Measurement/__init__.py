import json
import numpy as np
import hashlib
import datetime

from .Angular import Angular
from .Spectra import Spectra
from .Extensions.jsonsupport import JSONDecoder, JSONEncoder

__all__ = ['Measurement']


class Measurement(object):
    """
    :type material: str
    :type date_measure: datatime.data
    """
    def __init__(self):
        self.material = None
        self.short_desc = None
        self.desc = None
        self.date_measure = None
        self.authors = None
        self.spectra = None
        self.angular = None

    def __str__(self):
        return (
            'Measurement\n'.format() +
            '{:>16} {}\n'.format('material:', self.material) +
            '{:>16} {}\n'.format('short_desc:', self.short_desc) +
            '{:>16} {}\n'.format('date_measure:', self.date_measure.strftime('%d/%m/%Y')) +
            '{:>16} {}\n'.format('authors:', self.authors) +
            str(self.spectra) +
            str(self.angular)
        )

    def toJSON(self):
        return {
            'material': self.material,
            'authors': self.authors,
            'short_desc': self.short_desc,
            'desc': self.desc,
            'date_measure': self.date_measure.strftime('%d/%m/%Y'),
            'spectra': self.spectra.__dict__(),
            'angular': self.angular.__dict__()
        }

    def unique_name(self):
        m = hashlib.md5()
        m.update(self.material.encode('utf-8'))
        m.update(self.short_desc.encode('utf-8'))
        m.update(self.date_measure.strftime('%d/%m/%Y').encode('utf-8'))

        return m.hexdigest()
        # return 2

    def validate(self):
        assert self.material is not None

        assert self.angular.validate()
        assert self.spectra.validate()

        return True


    def load_json(self, filename):

        with open(filename, mode='r') as file:
            data = json.load(fp=file)   # type: dict

        self.material = data.pop('material', None)
        self.date_measure = datetime.datetime.strptime(data.pop('date_measure', None), '%d/%m/%Y').date()
        self.short_desc = data.pop('short_desc', None)
        self.desc = data.pop('desc', None)
        self.angular = Angular(**data.pop('angular', None))
        self.spectra = Spectra(**data.pop('spectra', None))



    def save_json(self, filename, *args, **kwargs):
        with open(filename, mode='w') as file:
            json.dump(self.toJSON(), file, *args, **kwargs)
