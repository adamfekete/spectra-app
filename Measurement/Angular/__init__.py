import numpy as np


class Angular(object):
    """Angular data

    :param x: wavelength - float array [N,1]
    :type x: np.ndarray
    :param y: wavelength - float array [M,1]
    :type y: np.ndarray
    :param I: intensity - float array [N,M]
    :type I: np.ndarray
    """

    def __init__(self, x, y, I):
        self.x = np.array(x)
        self.y = np.array(y)
        self.I = np.array(I)

    def plot(self):
        pass

    def validate(self):
        return True

    def __str__(self):
        return '{:>16}: [{}, {}]\n'.format('angular data', self.I.shape[0], self.I.shape[1])

    def __dict__(self):
        return {
            'x': self.x.tolist(),
            'y': self.y.tolist(),
            'I': self.I.tolist()
        }


