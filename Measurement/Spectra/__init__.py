import numpy as np


class Spectra(object):
    """Spectral data

    :param l: wavelength - float array [N,1]
    :type wavelength: np.ndarray
    :param I: intensity - float array [N,1]
    :type intensity: np.ndarray
    """
    def __init__(self, wavelength, intensity):
        self.wavelength = np.array(wavelength)
        self.intensity = np.array(intensity)


    def plot(self):
        pass

    def validate(self):
        assert self.wavelength.ndim == 2  # 2D array
        assert self.wavelength.shape[1] == 1  # vector
        assert self.wavelength.shape == self.intensity.shape  # same shape

        return True


    def __str__(self):
        return '{:>16}: [{}, {}]\n'.format('spectral data', self.intensity.shape[0], self.intensity.shape[1])

    def __dict__(self):
        return {
            'wavelength': self.wavelength.tolist(),
            'intensity': self.intensity.tolist()
        }

