import numpy as np
import datetime

from pprint import pprint

from Measurement import Measurement
from Measurement.Angular import Angular
from Measurement.Spectra import Spectra
from Measurement.Extensions.jsonsupport import JSONEncoder

import json



if __name__ == '__main__':
    # size of the random data
    n = 1000
    m = 1000

    meas = Measurement()
    meas.material = "DiamondDiamond"
    meas.authors = "Peter Pan, Jim Carter"
    meas.short_desc = "V = 1eV, I= 12 mA, perfect conditions"
    meas.desc = """Lorem Ipsum is simply dummy text of the printing and typesetting industry.

            Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
            electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release
            of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
            Aldus PageMaker including versions of Lorem Ipsum."""

    meas.date_measure = datetime.datetime.strptime('21/02/2016', '%d/%m/%Y').date()

    meas.spectra = Spectra(
        wavelength=np.linspace(10, 20, n)[:, np.newaxis],
        intensity=np.random.rand(n, 1)
    )

    meas.angular = Angular(
        x=np.linspace(1, 20, n)[:, np.newaxis],
        y=np.linspace(100, 200, m)[:, np.newaxis],
        I=np.random.rand(n, m)
    )

    print(meas)

    print(meas.validate())


    print(meas.unique_name())

    print(meas.save_json('data/test.json'))

    new = Measurement()
    new.load_json('data/test.json')


    print(new)


'''
Json format:{
    material: string - name of the material (main key for storing the data),
    short_desc: string - short description (shown in the database table),
    desc: string - detailed description of the measurement
    date_measure: date [day/month/year] - date of measurement,
    authors: string - name of persons,
    spectrum:{
        lambda: float array [N,1] - lambda,
        I: float array [N,1] - intensity
    }
    angular:{
        x: float array [N,1] - ,
        y: float array [M,1] - ,
        I: float array [N,M] - intensity
    }

}
'''
