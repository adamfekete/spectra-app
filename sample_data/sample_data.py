import numpy as np

import time
import timeit

import h5py

import scipy.io
import collections

import codecs, json


def timing(f):
    def wrap(*args):
        time1 = time.time()
        for i in range(10):
            ret = f(*args)
        time2 = time.time()
        print('%s function took %0.3f ms' % (f.__name__, (time2-time1)*1000.0))
        return ret
    return wrap


def read_data():
    filename = 'sample_data/angular_intensity.mat'
    mat = scipy.io.loadmat(filename)
    # print(mat)
    # print(mat.keys())
    vq = mat['vq']

    # print(np.nanmax(vq))
    # print(np.nanargmax(vq))
    # print(np.unravel_index(np.nanargmax(vq), vq.shape))
    vq[np.unravel_index(np.nanargmax(vq), vq.shape)] = 0
    # print(np.nanmax(vq))

    vq = vq


    filename = 'sample_data/angular_x.mat'
    mat = scipy.io.loadmat(filename)
    # print(mat.keys())
    xq = mat['xq'][0, :]


    filename = 'sample_data/angular_y.mat'
    mat = scipy.io.loadmat(filename)
    # print(mat.keys())
    yq = mat['yq'][:, 0]



    filename = 'sample_data/Spectrum_Intensity.mat'
    mat = scipy.io.loadmat(filename)
    # print(mat.keys())
    intensity = mat['Intensity']

    filename = 'sample_data/Spectrum_wavelength.mat'
    mat = scipy.io.loadmat(filename)
    # print(mat.keys())

    # for index, item in enumerate(mat['spec'][0, 0]):
        # print(index, item)

    # print(mat['spec'][0, 0][7].shape)
    # print(mat['spec'][0, 0][9].shape)

    wavelength = mat['spec'][0, 0][7]
    intensity = mat['spec'][0, 0][9]

    return wavelength, intensity, xq, yq, vq

# @timing
def save_mat(wavelength, intensity, xq, yq, vq):

    matlab_data = collections.defaultdict(dict)
    matlab_data["material"] = "CL_Single Spectrum"
    matlab_data["authors"] = "Sandro Mignuzzi"
    matlab_data["short_desc"] = "section9cavity1 90kx 30kV int15 t60s centre550nm slit100um rep1"
    matlab_data["date_measure"] = '21/02/2016'

    matlab_data["spectral_lambda"] = wavelength
    matlab_data["spectral_I"] = intensity


    matlab_data["angular_x"] = xq
    matlab_data["angular_y"] = yq
    matlab_data["angular_I"] = vq

    matlab_data["desc"] = """Lorem Ipsum is simply dummy text of the printing and typesetting industry.,

    Lorem Ipsum has been the
    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
    scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
    electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release
    of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
    Aldus PageMaker including versions of Lorem Ipsum."""

    scipy.io.savemat('sample_data/sample_data.mat', matlab_data, appendmat=False, do_compression=True)
    #
    #
    # print(scipy.io.loadmat('sample_data.mat', squeeze_me=True, chars_as_strings=True, ))

# @timing
def save_hdf5(wavelength, intensity, xq, yq, vq):
    f = h5py.File("sample_data/sample_data.h5", "w")

    # dset = f.create_dataset("spectral_data/lambda", wavelength.flatten().shape, compression="gzip")
    dset = f.create_dataset("spectral_data/lambda", wavelength.flatten().shape)
    dset[...] = wavelength.flatten()

    # dset = f.create_dataset("spectral_data/intensity", intensity.flatten().shape, compression="gzip")
    dset = f.create_dataset("spectral_data/intensity", intensity.flatten().shape)
    dset[...] = intensity.flatten()

    # dset = f.create_dataset("angular_data/x", xq.shape, compression="gzip")
    dset = f.create_dataset("angular_data/x", xq.shape)
    dset[...] = xq
    # dset = f.create_dataset("angular_data/y", yq.shape, compression="gzip")
    dset = f.create_dataset("angular_data/y", yq.shape)
    dset[...] = yq
    # dset = f.create_dataset("angular_data/intensity", vq.shape, compression="gzip")
    dset = f.create_dataset("angular_data/intensity", vq.shape)
    dset[...] = vq


    f.attrs["name"] = u"Hello"
    f.attrs["material"] = u"CL_Single Spectrum"
    f.attrs["authors"] = u"Sandro Mignuzzi"
    f.attrs["short_desc"] = u"section9cavity1 90kx 30kV int15 t60s centre550nm slit100um rep1"
    f.attrs["date_measure"] = u'21/02/2016'

    f.attrs["desc"] = u"""Lorem Ipsum is simply dummy text of the printing and typesetting industry.,

    Lorem Ipsum has been the
    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
    scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
    electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release
    of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
    Aldus PageMaker including versions of Lorem Ipsum."""

    f.close()


def save_json(wavelength, intensity, xq, yq, vq):

    matlab_data = collections.defaultdict(dict)
    matlab_data["material"] = "CL_Single Spectrum"
    matlab_data["authors"] = "Sandro Mignuzzi"
    matlab_data["short_desc"] = "section9cavity1 90kx 30kV int15 t60s centre550nm slit100um rep1"
    matlab_data["date_measure"] = '21/02/2016'

    matlab_data["spectral_lambda"] = wavelength.flatten().tolist()
    matlab_data["spectral_I"] = intensity.flatten().tolist()

    matlab_data["angular_x"] = xq.tolist()
    matlab_data["angular_y"] = yq.tolist()
    matlab_data["angular_I"] = vq.tolist()

    matlab_data["desc"] = """Lorem Ipsum is simply dummy text of the printing and typesetting industry.,

    Lorem Ipsum has been the
    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
    scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
    electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release
    of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
    Aldus PageMaker including versions of Lorem Ipsum."""

    file_path = "sample_data/sample_data.json" ## your path variable

    json.dump(matlab_data, codecs.open(file_path, 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4) ### this saves the array in .json format
    # json.dump(matlab_data, codecs.open(file_path, 'w', encoding='utf-8')) ### this saves the array in .json format



if __name__ == '__main__':

    wavelength, intensity, xq, yq, vq = read_data()

    # save_mat(wavelength, intensity, xq, yq, vq)
    # save_hdf5(wavelength, intensity, xq, yq, vq)

    print(timeit.timeit(lambda: save_mat(wavelength, intensity, xq, yq, vq), number=1))
    print(timeit.timeit(lambda: save_hdf5(wavelength, intensity, xq, yq, vq), number=1))
    print(timeit.timeit(lambda: save_json(wavelength, intensity, xq, yq, vq), number=1))

    # print(scipy.io.loadmat('sample_data.mat', squeeze_me=True, chars_as_strings=True, ))

    f = h5py.File("sample_data/sample_data.h5", "r")
    try:
       dataset_three = f['subgroup2/dataset_three']
    except KeyError:
        pass

    print(f)

    # print(matlab_data)

    # scipy.io.savemat('data/sample_data.mat', matlab_data, appendmat=False, do_compression=True)


    # print(scipy.io.loadmat('data/sample_data.mat'))


    #
    # data = Measurement()
    #
    # data.material = "CL_Single Spectrum"
    # data.authors = "Sandro Mignuzzi"
    # data.short_desc = "V = 1eV, I= 12 mA, perfect conditions"
    # data.desc = """Lorem Ipsum is simply dummy text of the printing and typesetting industry.
    #
    #         Lorem Ipsum has been the
    #         industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
    #         scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
    #         electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release
    #         of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
    #         Aldus PageMaker including versions of Lorem Ipsum."""
    #
    # data.date_measure = datetime.datetime.strptime('21/02/2016', '%d/%m/%Y').date()
    #
    # data.spectra = Spectra(
    #     wavelength=wavelength.flatten(),
    #     intensity=intensity.flatten()
    # )
    #
    # data.angular = Angular(
    #     x=xq.flatten(),
    #     y=yq.flatten(),
    #     I=np.asarray(vq)
    # )
    #
    # print(data.save_json('sample_data/sample_data.json'))
    #
    #
    # print(data)
    #
    # print(data.validate())
    #
    #
    # print(data.unique_name())
    # #
    #
    # scipy.io.savemat('data/sample_data.mat', data.toJSON(), appendmat=False, do_compression=True)
    #
    #
    # print(scipy.io.loadmat('data/sample_data.mat'))
    # #
    # import numpy as np
    # import matplotlib.pyplot as plt
    #
    # # -- Generate Data -----------------------------------------
    # # Using linspace so that the endpoint of 360 is included...
    # azimuths = np.radians(np.linspace(0, 360, 40))
    # zeniths = np.linspace(0, 70, 100)
    #
    # r, theta = np.meshgrid(zeniths, azimuths)
    # values = np.random.random((azimuths.size, zeniths.size))
    #
    #
    # # -- Plot... ------------------------------------------------
    # fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
    # ax.contour(theta, r, values)
    # ax.pcolor(theta, r, values)
    #
    # plt.show()
    # #
    # #
    # # import numpy as np
    # # import matplotlib.pyplot as plt
    # #
    # #
    # # x = xq
    # # y = yq
    # # z = np.rot90(np.nan_to_num(vq))
    # #
    # # z_min, z_max = z.min(), z.max()
    # #
    # # # plt.contour(xi, yi, zi, 15, linewidths = 0.5, colors = 'k')
    # # plt.pcolormesh(x, y, z, vmin=0, vmax=1.4e6)
    # #
    # # plt.colorbar()
    # # # plt.scatter(x, y, marker = 'o', c = 'b', s = 5, zorder = 10)
    # # # plt.xlim(xmin, xmax)
    # # # plt.ylim(ymin, ymax)
    # # plt.show()
    # #
    # # # print(wavelength.shape)
    # # # print(intensity.shape)
    # # #
    # # # plt.plot(wavelength, intensity)
    # # # plt.show()