Hi Adam,
 
I am sending the mat files for the
-          Spectral data: wavelength vs intensity
-          Angular data: Intensity vs azimuthal (theta) and zenithal (phi) angles
 
For the angular data the domain of the data is represented by 2 uniform meshgrids (matrix x and matrix y), where
X= theta*Cos(phi)
Y=theta*Sin(phi)
 
In attachment some typical angular patterns.
 
I hope this is clear, if not please ask.
 
Thanks
 
Sandro