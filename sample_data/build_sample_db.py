import random
import datetime

import sys
sys.path.append("../")

from app import app, db
from app.models.measurement import Measurement
from app.models.category import Category
from app.models.material import Material
from app.models.experiment import Experiment
from collections import namedtuple

from Measurement import Measurement as Data

from app.config import BASE_DIR

import uuid
import numpy as np
import os
import os.path as op

# app = create_app("config.py")


# def build_data():

from scipy.io import savemat, loadmat




def build_sample_db():
    """
    Populate a small db with some example entries.
    """



    db.drop_all()

    folder = op.join(BASE_DIR, 'files')
    for f in os.listdir(folder):
        if f.endswith(".mat"):
            os.remove(op.join(BASE_DIR, 'files', f))

    db.create_all()

    category_names = [
        'Semiconductor',
        'Metal',
        'Organic',
    ]

    category_list = []
    for name in category_names:
        cat = Category()
        cat.name = name
        category_list.append(cat)
        db.session.add(cat)


    material_names = [
        'Carbon',
        'Diamond',
        'Graphene1',
        'Graphene2',
        'Graphene3',
        'Graphene4',
    ]

    material_list = []
    for name in material_names:
        mat = Material()
        mat.name = name
        mat.category = random.sample(category_list, 1)[0]

        material_list.append(mat)

        db.session.add(mat)


    experiment_list = Experiment.query.all()


    author_name = [
        'John Doe',
        'Scherlock Holmes'
    ]


    sample_parameters = [
        "Haec dicuntur inconstantissime. Quae cum dixisset paulumque institisset, Quid est?",
        "Restatis igitur vos; Ita prorsus, inquam; Comprehensum, quod cognitum non habet? Confecta res esset. Istic sum, inquit.",
        "Aperiendum est igitur, quid sit voluptas; Duo enim genera quae erant, fecit tria. Restatis igitur vos; Quid ait Aristoteles reliquique Platonis alumni? Si longus, levis dictata sunt.",
        "Haeret in salebra. Quonam, inquit, modo? Summae mihi videtur inscitiae. Nihil ad rem! Ne sit sane;"
    ]


    sample_description = [
        "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor \
        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud \
        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure \
        dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt \
        mollit anim id est laborum.",
        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque \
        laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto \
        beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur \
        aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi \
        nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, \
        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam \
        aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam \
        corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum \
        iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum \
        qui dolorem eum fugiat quo voluptas nulla pariatur?",
        "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium \
        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati \
        cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id \
        est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam \
        libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod \
        maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. \
        Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet \
        ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur \
        a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis \
        doloribus asperiores repellat."
    ]

    n, m = 100, 100
    data = loadmat('sample_data.mat', squeeze_me=True)

    print(data)

    data_spectra = data.copy()
    data_spectra.pop('angular_x', None)
    data_spectra.pop('angular_y', None)
    data_spectra.pop('angular_I', None)

    data_angular = data.copy()
    data_angular.pop('spectral_lambda', None)
    data_angular.pop('spectral_I', None)

    for i in range(100):
        measurement = Measurement()
        # measurement.category = random.sample(category_list, 1)[0]
        measurement.authors = random.sample(author_name, 1)[0]
        measurement.parameters = random.sample(sample_parameters, 1)[0]
        measurement.description = random.sample(sample_description, 1)[0]
        measurement.material = random.sample(material_list, 1)[0]
        measurement.date_measure = datetime.datetime.strptime('21/02/2016', '%d/%m/%Y').date()

        unique_id = uuid.uuid4()

        output_data = random.sample([data, data_spectra, data_angular], 1)[0]
        measurement.experiments = random.sample(experiment_list, 1)
        savemat('../app/files/{}.mat'.format(unique_id), output_data)


        # spectra ={
        #     "wavelength": np.linspace(10, 20, n)[:, np.newaxis],
        #     "intensity": np.random.rand(n, 1)
        # }
        #
        # savemat('app/files/{}.mat'.format(unique_id), spectra, appendmat=False)
        #
        # angular = {
        #     "x": np.linspace(1, 20, n)[:, np.newaxis],
        #     "y": np.linspace(100, 200, m)[:, np.newaxis],
        #     "z": np.random.rand(n, m)
        # }
        #
        # savemat('app/files/{}.mat'.format(unique_id), angular, appendmat=True)

        measurement.tags = random.sample(experiment_list, 1)
        measurement.filename = '{}.mat'.format(unique_id)

        db.session.add(measurement)


    db.session.commit()


if __name__ == '__main__':
    with app.app_context():
        build_sample_db()

