import os
import os.path as op

from app import db
from app.models.material import Material
from app.models.experiment import Experiment
from app.config import BASE_DIR
from sqlalchemy import event

tags = db.Table(
    'tags',
    db.Column('experiment_id', db.Integer, db.ForeignKey('experiment.id'), primary_key=True),
    db.Column('measurement_id', db.Integer, db.ForeignKey('measurement.id'), primary_key=True)
)


class Base(db.Model):

    __abstract__  = True

    id           = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp())

    authors      = db.Column(db.String(120))
    parameters   = db.Column(db.Text)
    description  = db.Column(db.Text)
    date_measure = db.Column(db.Date)

    filename     = db.Column(db.Unicode(128))


class Measurement(Base):
    # , passive_deletes = 'all', nullable=False
    material_id = db.Column(db.Integer, db.ForeignKey('material.id'))
    material = db.relationship(Material, backref=db.backref('measurements', lazy='dynamic'), order_by=Material.name)

    # category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    # category = db.relationship(Category, backref=db.backref('measurements', lazy='dynamic'))

    experiments = db.relationship(Experiment, secondary=tags, lazy='subquery', backref=db.backref('experiment', lazy=True))

    def __str__(self):
        return self.material if self.material else ''


    def __repr__(self):
        return '<Measurement {} ({})>'.format(self.material, self.parameters)


# Delete hooks for models, delete files if models are getting deleted
@event.listens_for(Measurement, 'after_delete')
def del_file(mapper, connection, target):
    if target.filename:
        try:
            os.remove(op.join(BASE_DIR, 'files', target.filename))
        except OSError:
            # Don't care if was not deleted because it does not exist
            pass














#     @staticmethod
#     def generate_slug(target, value, oldvalue, initiator):
#         if value and (not target.slug or value != oldvalue):
#             target.slug = slugify(value)
#
#
# # https://www.michaelcho.me/article/using-model-callbacks-in-sqlalchemy-to-generate-slugs
# event.listen(Experiment.name, 'set', Experiment.generate_slug, retval=False)

# https://www.michaelcho.me/article/using-model-callbacks-in-sqlalchemy-to-generate-slugs

# class SpectralData(Base):
#
#     material_id = db.Column(db.Integer, db.ForeignKey('material.id'))
#     material = db.relationship(Material, backref=db.backref('spectraldata', lazy='dynamic'))
#
#
# class AngularData(Base):
#
#     material_id = db.Column(db.Integer, db.ForeignKey('material.id'))
#     material = db.relationship(Material, backref=db.backref('angulardata', lazy='dynamic'))
#


    # description  = db.Column(db.Text)
    # datetime.datetime.strptime('21/02/2016', '%d/%m/%Y').date()

    # def __init__(self, material, authors, excerpt, date_measure, filename):
    #
    #     self.material     = material            # name of material
    #     self.authors      = authors             # authors
    #     self.date_measure = date_measure        # date of measurement
    #     self.excerpt      = excerpt             # parameters, short description
    #     self.filename     = filename            # relative path and filename

    # @property
    # def haha(self):
    #     return 'haha'
    #
    # def hello(self):
    #     pass



    # @staticmethod
    # def define_tags(target, value, oldvalue, initiator):
    #     from scipy.io import loadmat
    #     print(loadmat('files/'+value))
    #     pass

# event.listen(Measurement.filename, 'set', Measurement.define_tags, retval=False)
