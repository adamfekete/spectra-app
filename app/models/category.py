from app import db
from sqlalchemy import event
from slugify import slugify


# Define a Category model
class Category(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128),  nullable=False)
    slug = db.Column(db.String(128),  nullable=False, unique=True)

    def __init__(self, name=''):
        self.name = name

    def __repr__(self):
        return '<Category {}>'.format(self.name)

    def __str__(self):
        return self.name

    @property
    def count(self):
        return self.materials.count()


@event.listens_for(Category.__table__, 'after_create')
def insert_initial_values(*args, **kwargs):
    db.session.add(Category(name='Uncategorised'))
    db.session.commit()


@event.listens_for(Category.name, 'set', retval=False)
def generate_slug(target, value, oldvalue, initiator):
    if value and (not target.slug or value != oldvalue):
        target.slug = slugify(value)

