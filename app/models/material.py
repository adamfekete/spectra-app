from app import db
from sqlalchemy import event
from slugify import slugify

from app.models.category import Category


# Define a Material model
class Material(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    slug = db.Column(db.String(128), index=True, unique=True)

    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship(Category, backref=db.backref('materials', lazy='dynamic'))

    # New instance instantiation procedure
    def __init__(self, name=''):
        self.name     = name

    def __repr__(self):
        return '<Material {}>'.format(self.name)

    def __str__(self):
        return self.name

    @property
    def count(self):
        return self.measurements.count()



@event.listens_for(Material.name, 'set', retval=False)
def generate_slug(target, value, oldvalue, initiator):
    if value and (not target.slug or value != oldvalue):
        target.slug = slugify(value)

