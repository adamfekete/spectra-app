from app import db
from sqlalchemy import event
from slugify import slugify


# Define a Experiment model
class Experiment(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(255), nullable=False)
    slug = db.Column(db.String(128), index=True, unique=True)

    def __init__(self, name=''):
        self.name = name

    def __repr__(self):
        return '<Experiment {}>'.format(self.name)

    def __str__(self):
        return self.name


@event.listens_for(Experiment.__table__, 'after_create')
def insert_initial_values(*args, **kwargs):
    db.session.add(Experiment(name='Angular data'))
    db.session.add(Experiment(name='Spectral data'))
    db.session.commit()


@event.listens_for(Experiment.name, 'set', retval=False)
def generate_slug(target, value, oldvalue, initiator):
    if value and (not target.slug or value != oldvalue):
        target.slug = slugify(value)

