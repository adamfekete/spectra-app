#  Import flask and template operators
from flask import Flask, render_template

# Import extensions
from flask_appconfig import AppConfig
from flask_bootstrap import Bootstrap
from flask_debug import Debug
# from flask_markdown import Markdown
from flaskext.markdown import Markdown


from app.db import db
from app.nav import nav
from app.auth import basic_auth

# Import database models
from app.models.category import Category
# from app.models.material import Material
# from app.models.experiment import Experiment
# from app.models.measurement import Measurement, Experiment
# from models.measurement import SpectralData, AngularData

# Import a module / component using its blueprint handler variable (mod_auth)
from app.views.index import frontend
from app.views.measurements import measurements
from app.views.materials import materials
from app.views.api import api
from app.views.search import search

# ============================================================================
# Initialize app. Flatten config_obj to dictionary (resolve properties).
# ============================================================================

# Define the WSGI application object
app = Flask(__name__)


# ============================================================================
# Initialize extensions/add-ons/plugins.
# ============================================================================

from app.admin import admin

# Configurations: The usage of Flask-Appconfig could be useful, but not a requirement.
# app.config.from_object('config')

AppConfig(app, 'config.py')
Bootstrap(app)              # Install our Bootstrap extension
Markdown(app)

app.debug and Debug(app)    # Enable debug interface


db.init_app(app)            # Define the database object
nav.init_app(app)           # We initialize the navigation as well
admin.init_app(app)
basic_auth.init_app(app)


# ============================================================================
# Build the database
# ============================================================================


with app.app_context():
    # # This will create the database file using SQLAlchemy
    db.create_all()


# ============================================================================
# Setup redirects and register blueprints.
# ============================================================================


app.add_url_rule('/favicon.ico', 'favicon', lambda: app.send_static_file('favicon.ico'))


# Our application uses blueprints as well; these go well with the
# application factory. We already imported the blueprint, now we just need
# to register it:
app.register_blueprint(frontend)
app.register_blueprint(materials)
app.register_blueprint(measurements)
app.register_blueprint(api)
app.register_blueprint(search)

# Because we're security-conscious developers, we also hard-code disabling
# the CDN support (this might become a default in later versions):
app.config['BOOTSTRAP_SERVE_LOCAL'] = True


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


from app.forms.search import SearchForm
from flask import g

@app.before_request
def before_request():
    g.search_form = SearchForm()

# __all__ = [Category, Measurement, Material, db]



# Migration: https://danidee10.github.io/2016/10/05/flask-by-example-5.html