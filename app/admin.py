from flask import Response, redirect, url_for, request, flash, current_app
from flask_admin import Admin, form
from flask_admin import BaseView, expose
from flask_admin.menu import MenuLink
from flask_admin.contrib import sqla
from flask_admin import BaseView, expose
from flask_admin.model.base import ValidationError
from flask_admin.actions import action
from flask_admin.form.upload import secure_filename, FileUploadInput
from werkzeug.exceptions import HTTPException
# from werkzeug import secure_filename
from flask_admin.form import BaseForm

from scipy.io import whosmat

import os
import os.path as op
import uuid

from app import db, basic_auth
from app.models.category import Category
from app.models.material import Material
from app.models.experiment import Experiment
from app.models.measurement import Measurement
# from app import SpectralData, AngularData


admin = Admin(None, name='Admin Page', template_mode='bootstrap3')


# Widgets
class FileUploadInput(FileUploadInput):
    empty_template = '<input %(file)s>'
    data_template = '<input class="form-control" %(text)s>'


class FileUploadField(form.FileUploadField):
    widget = FileUploadInput()


# Create directory for file fields to use
file_path = op.join(op.dirname(__file__), 'files')
try:
    os.mkdir(file_path)
except OSError:
    pass


# http://computableverse.com/blog/flask-admin-using-basicauth/
class ModelView(sqla.ModelView):
    def is_accessible(self):
        if not basic_auth.authenticate():
            raise AuthException('Not authenticated.')
        else:
            return True

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(basic_auth.challenge())


class AuthException(HTTPException):
    def __init__(self, message):
        super().__init__(message, Response(
            "You could not be authenticated. Please refresh the page.", 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'}
        ))


class CategoryView(ModelView):

    column_display_pk = True

    can_edit = True  # disable model deletion
    form_columns = ['name']

    column_list = ("id", "name", "slug", "count")


    # def get_query(self):
    #     return (
    #         self.session.query(
    #             Category.name.label("name"),
    #             func.count(Document.id).label("n_documents")
    #         )
    #             .join(user_document_table)
    #             .join(Document)
    #             .group_by(User.id)
    #     )

    # @action('approve', 'Approve', 'Are you sure you want to approve selected users?')
    # def action_approve(self, ids):
    #
    #     try:
    #         query = User.query.filter(User.id.in_(ids))
    #
    #         count = 0
    #         for user in query.all():
    #             if user.approve():
    #                 count += 1
    #
    #         flash(ngettext('User was successfully approved.',
    #                        '%(count)s users were successfully approved.',
    #                        count,
    #                        count=count))
    #     except Exception as ex:
    #         if not self.handle_view_exception(ex):
    #             raise
    #
    #     flash('Failed to approve users.')


    def on_model_delete(self, model):

        if model.id is 1:
            raise ValidationError('The "root" is not removable.')

        if model.materials.count():
            raise ValidationError('The category is not empty.')




class MaterialView(ModelView):

    column_display_pk = True

    can_edit = True  # disable model deletion
    form_columns = ['name', 'category']

    column_list = ("id", "name", "category", "slug", "count")
    column_sortable_list = ('id', ('category', 'category.name'), 'name', "slug", "count")

    # column_list = ('id', 'material', 'experiments', 'parameters', 'authors', 'date_measure',)
    # column_searchable_list = ('authors', 'parameters', 'material.name', 'experiments.name')
    # column_default_sort = ('date_measure', True)
    # # column_filters = ('category.name', 'experiments')



    # use Model method instead
    # def on_model_change(self, form, model, is_created):
    #     from slugify import slugify
    #
    #     # if is_created and not model.slug:
    #     model.slug = slugify(model.name)

    def on_model_delete(self, model):

        if model.measurements.count():
            raise ValidationError('The category is not empty.')


class ExperimentView(ModelView):

    column_display_pk = True

    can_create = False
    can_edit = False  # disable model deletion
    can_delete = False

    # form_columns = ['name']


class BaseView(ModelView):

    column_display_pk = True
    can_edit = True  # disable model deletion

    column_list = ('material', 'excerpt', 'authors', 'date_measure',)
    column_searchable_list = ('authors', 'excerpt',)
    column_default_sort = ('date_measure', True)
    column_filters = ('material',)
    column_sortable_list = ('authors', 'excerpt', 'date_measure',)

    form_columns = ['material', 'excerpt', 'description', 'filename', 'authors', 'date_measure']



class SpectralDataView(BaseView):

    def on_model_change(self, form, model, is_created):
        pass


class AngularDataView(BaseView):

    def on_model_change(self, form, model, is_created):
        pass



def prefix_name(obj: Measurement, file_data):
    # generate unique id for filename
    print('start prefix')
    # print(obj)
    # print(file_data)
    print('prefix')
    unique_id = uuid.uuid4()
    # obj.excerpt = 'haho'
    return secure_filename('{}-{}'.format(unique_id, file_data.filename))


def validator(form, field):
    from scipy.io import loadmat




    print('start validator')
    print(form)
    print(field)
    print('validatro')
    # form.authors.data = 'hello'

    return True


# Administrative views
class MeasurementView(ModelView):

    # column_display_pk = True

    column_list = ('id', 'material', 'experiments', 'parameters', 'authors', 'date_measure',)
    column_searchable_list = ('authors', 'parameters', 'material.name', 'experiments.name')
    column_default_sort = ('date_measure', True)
    # column_filters = ('category.name', 'experiments')
    column_sortable_list = ('id', ('material', 'material.name'), 'authors', 'parameters', 'date_measure',)

    form_columns = ['material', 'experiments', 'parameters', 'description', 'filename', 'authors', 'date_measure']

    # Override form field to use Flask-Admin FileUploadField
    form_overrides = {
        'filename': FileUploadField
    }

    from wtforms import validators

    # Pass additional parameters to 'path' to FileUploadField constructor
    form_args = {
        'filename': {
            'label': 'File',
            'base_path': file_path,
            'allowed_extensions': ['mat'],
            'validators': [validator, validators.required()],
            'namegen': prefix_name,
            'allow_overwrite': False
        }
    }

    form_widget_args = {
        'description': {
            'rows':20
            # 'style': 'height: 300 px'
        }
    }


    can_export = True
    # export_types = ['json']

    def on_model_change(self, form, model, is_created=False):

        experiments_map = {
            'angular-data': ['angular_x','angular_y','angular_I'],
            'spectral-data': ['spectral_lambda', 'spectral_I']
        }

        if is_created:
            file_path = op.join(current_app.root_path, 'files', form.filename.data.filename)
        else:
            file_path = op.join(current_app.root_path, 'files', model.filename)

        varialbe_list = [var[0] for var in whosmat(file_path)]


        for exp in form.experiments.data:
            if not all(x in varialbe_list for x in experiments_map[exp.slug]):
                flash('The "{}" was selected, but only the following variables are available:\n{}'.format(exp.name, varialbe_list), category='warning')


# import os.path as op
#
# def prefix_name1(obj, file_data):
#     parts = op.splitext(file_data.filename)
#     return secure_filename('file-%s%s' % parts)
#
#
#
# class MyForm(BaseForm):
#     upload = FileUploadField('File111', namegen=prefix_name1)


class AnalyticsView(BaseView):
    @expose('/')
    def index(self):
        return self.render('admin/analytics_index.html')




admin.add_view(CategoryView(Category, db.session, menu_icon_type='glyph', menu_icon_value='glyphicon-tags'))
admin.add_view(MaterialView(Material, db.session, menu_icon_type='glyph', menu_icon_value='glyphicon-th-list'))
admin.add_view(ExperimentView(Experiment, db.session, menu_icon_type='glyph', menu_icon_value='glyphicon-tags'))
admin.add_view(MeasurementView(Measurement, db.session, menu_icon_type='glyph', menu_icon_value='glyphicon-file'))
# admin.add_view(SpectralDataView(SpectralData, db.session, menu_icon_type='glyph', menu_icon_value='glyphicon-file'))
# admin.add_view(AngularDataView(AngularData, db.session, menu_icon_type='glyph', menu_icon_value='glyphicon-file'))
# admin.add_view(AnalyticsView(name='Analytics', endpoint='analytics'))

# Add home link by url
admin.add_link(MenuLink(name='Back to home', url='/', icon_type='glyph', icon_value='glyphicon-home'))



#
# class MyView(BaseView):
#     @expose('/')
#     def index(self):
#         return redirect(url_for('frontend.index'))
#
# admin.add_view(MyView(name='Back to the main page', menu_icon_type='glyph', menu_icon_value='glyphicon-home'))
