import os


# Statement for enabling the development environment
DEBUG = True


# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))


# Define the database - we are working with SQLite for this example
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False
DATABASE_CONNECT_OPTIONS = {}


# Use a secure, unique and absolutely secret key for signing the data.
CSRF_SESSION_KEY = "secret_key"


# Secret key for signing cookies
SECRET_KEY = "secret_key"


# Username and password for the Flask-BasicAuth extensions (using for Admin page)
BASIC_AUTH_USERNAME = 'user'
BASIC_AUTH_PASSWORD = 'password'


# FLASK_DEBUG_DISABLE_STRICT = True
