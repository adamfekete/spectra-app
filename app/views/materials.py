from flask import Blueprint, render_template, Markup, current_app, request
from app.models.measurement import Measurement
from app.models.material import Material
from app.models.category import Category

from flask_sqlalchemy import Pagination

materials = Blueprint('materials', __name__, url_prefix='/materials')

ELEMENTS_PER_PAGE = 10


@materials.route('/')
def index():

    cat_list = Category.query.filter(Category.id != 1).order_by(Category.name).all()

    content = []
    for category in cat_list:

        data = []
        for material in Material.query.filter(Material.category == category).order_by(Material.name).all():
            data += Measurement.query.filter(Measurement.material == material).all()


        content.append(dict(
            category=category,
            measurements=data
        ))

    return render_template(
        'materials/materials.html',
        categories=cat_list,
        content=content,
    )



@materials.route('/<string:category_slug>', methods=['POST', 'GET'])
def by_category(category_slug):

    page = request.args.get('page', 1, type=int)

    category = Category.query.filter_by(slug=category_slug).filter(Category.id != 1).first_or_404()
    #
    # data = []
    # for material in Material.query.filter(Material.category == category).order_by(Material.name).all():
    #     data += Measurement.query.filter(Measurement.material == material).all()

    query_results = Measurement.query\
        .outerjoin(Material) \
        .filter(Material.category == category)\
        .order_by(Material.name)

    return render_template(
        'category/materials.html',
        category=category,
        # content=data,
        query_results=query_results.paginate(page, ELEMENTS_PER_PAGE, True)
    )





