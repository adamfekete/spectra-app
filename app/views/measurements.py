import os.path as op

from flask import Blueprint, render_template, Markup, current_app, request
from app.models.measurement import Measurement
from app.models.material import Material
from app.models.experiment import Experiment
from app.config import BASE_DIR

import numpy as np
from scipy.io import loadmat

# https://plot.ly/python/reference/
# import plotly.offline as py
import app.utils.plotly_override as py
import plotly.graph_objs as go


measurements = Blueprint('measurements', __name__, url_prefix='/measurement')

#
# @measurements.route('/')
# def measurement():
#     materials = Material.query.all()
#
#     return render_template(
#         'measurements/measurements.html',
#         materials=materials)





@measurements.route('/<int:measurement_id>')
def show_measurement(measurement_id):
    # show the post with the given id, the id is an integer

    measurement_data = Measurement.query.get_or_404(measurement_id)

    experiments = {
        'angular': Experiment.query.get(1),
        'spectral': Experiment.query.get(2),
    }


    plot_div = dict()


    # file_path = op.join(BASE_DIR, 'files', measurement.filename)
    file_path = op.join(current_app.root_path, 'files', measurement_data.filename)

    if op.isfile(file_path):

        data = loadmat(file_path, squeeze_me=True, struct_as_record=True)

        if experiments['spectral'] in measurement_data.experiments \
                and data.keys() & {'spectral_lambda', 'spectral_I'}:

            sl = data['spectral_lambda']
            si = data['spectral_I']

            plot_div['spectral'] = generate_spectral_plot(sl, si)

        if experiments['angular'] in measurement_data.experiments \
                and data.keys() & {'angular_x', 'angular_y', 'angular_I'}:

            plot_div['angular'] = generate_angular_plot(data['angular_x'], data['angular_y'], data['angular_I'])
            # plot_div['surf'] = generate_angular_surface(x, y, I)


    return render_template('measurements/measurement.html',
                           # spectral_data=spectral_data,
                           # spectral_layout=spectral_layout,
                           measurement=measurement_data,
                           # description=Markup(data['desc']),
                           div_placeholder=plot_div
                           )


@measurements.route('/test/plot.png')
def plot_test():
    from flask import make_response

    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure
    import random
    from io import BytesIO

    fig = Figure()
    axis = fig.add_subplot(1, 1, 1)

    xs = range(100)
    ys = [random.randint(1, 50) for x in xs]

    axis.plot(xs, ys)
    canvas = FigureCanvas(fig)
    output = BytesIO()
    # output = StringIO()  # python 2.7x
    canvas.print_png(output)
    response = make_response(output.getvalue())
    response.mimetype = 'image/png'
    return response


def generate_plot():
    import json
    import plotly

    import pandas as pd
    import numpy as np

    rng = pd.date_range('1/1/2011', periods=7500, freq='H')
    ts = pd.Series(np.random.randn(len(rng)), index=rng)

    graphs = [
        dict(
            data=[
                dict(
                    x=[1, 2, 3],
                    y=[10, 20, 30],
                    type='scatter'
                ),
            ],
            layout=dict(
                title='first graph'
            )
        ),

        dict(
            data=[
                dict(
                    x=[1, 3, 5],
                    y=[10, 50, 30],
                    type='bar'
                ),
            ],
            layout=dict(
                title='second graph'
            )
        ),

        dict(
            data=[
                dict(
                    x=ts.index,  # Can use the pandas data structures directly
                    y=ts
                )
            ]
        )
    ]

    # Add "ids" to each of the graphs to pass up to the client
    # for templating
    ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]

    # Convert the figures to JSON
    # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
    # objects to their JSON equivalents
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

    return ids, graphJSON

#
# def generate_spectral_plot(wavelength, intensity):
#     import json
#     import plotly
#
#     import numpy as np
#
#     data = dict(
#         x=np.asarray(wavelength),
#         y=intensity
#     )
#
#     layout = dict(
#         title='first graph'
#     )
#
#     # Convert the figures to JSON
#     # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
#     # objects to their JSON equivalents
#     return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder), json.dumps(layout, cls=plotly.utils.PlotlyJSONEncoder)


def generate_spectral_plot(wavelength, intensity):

    # plot(figure_or_data, show_link=True, link_text='Export to plot.ly', validate=True, output_type='file',
    #      include_plotlyjs=True, filename='temp-plot.html', auto_open=True, image=None, image_filename='plot_image',
    #      image_width=800, image_height=600)

    data = [
        go.Scatter(
            x=wavelength,
            y=intensity,
        )
    ]

    layout = go.Layout(
        title='',
        xaxis=dict(
            title='Wavelength (nm)',
            ticks='inside',
            showline=True,
        ),
        yaxis=dict(
            title="Intensity",
            showexponent='all',
            exponentformat='none',
            separatethousands=True,
            ticks='inside',
            showline=True,
        ),
        margin=dict(
            l=80,
            r=20,
            t=30,
            b=80,
            pad=10,
            autoexpand=True,
        ),
    )

    fig = go.Figure(data=data, layout=layout)

    mode_bar_buttons_to_remove = ['sendDataToCloud', 'zoomIn2d', 'zoomOut2d', 'resetScale2d', 'hoverClosestCartesian',
                                  'hoverCompareCartesian']

    # toggleSpikelines
    plot_div = py.plot(
        fig,
        output_type='div',
        include_plotlyjs=False,
        modeBarButtonsToRemove=mode_bar_buttons_to_remove
    )

    return plot_div



def generate_angular_plot(x, y, intensity):

    # plot(figure_or_data, show_link=True, link_text='Export to plot.ly', validate=True, output_type='file',
    #      include_plotlyjs=True, filename='temp-plot.html', auto_open=True, image=None, image_filename='plot_image',
    #      image_width=800, image_height=600)

    data = [
        go.Heatmap(
            x=x,
            y=y,
            z=intensity,
            type='heatmap',
            zsmooth='best',
            colorscale='Viridis',
            hoverinfo='z',
            transpose=True,
            colorbar=dict(
                title='Intensity',
                titleside='top',
                lenmode='fraction',
                len=0.8,
                showexponent='all',
                exponentformat='none',
                separatethousands=True,
                xanchor='left',
                xpad=20,
                x=1.0,
                yanchor='middle',
                ypad=10,
                y=0.5,
                ticks='inside',
            ),
        ),
        # go.Surface(
        #     x=x,
        #     y=y,
        #     z=intensity,
        # )
    ]

    phi_axis_ticks = [0, 60, 120, 180, 240, 300]
    theta_axis_ticks = [0, 30, 60, 90]

    axis_labels, axis_lines = [], []
    for tick in phi_axis_ticks:

        label = dict(
            visible=True,
            clicktoshow=False,
            opacity='1',
            # bgcolor='rgba(0, 0, 0, 0)',
            # bordercolor='rgba(0, 0, 0, 0)',
            # borderpad='1',
            # borderwidth='1',
            text='{}º'.format(tick),
            textangle=tick if tick < 90 or tick > 270 else 180+tick,
            align='center',
            xref='x',
            xanchor='center',
            x=1.7 * np.sin(tick * np.pi / 180.),
            ax=30 * np.sin(tick * np.pi / 180.),
            xshift='0',
            yref='y',
            yanchor='middle',
            y=1.7 * np.cos(tick * np.pi / 180.),
            ay=-30 * np.cos(tick * np.pi / 180.),
            yshift='0',
            showarrow=True,
            arrowhead=0,
            arrowsize=1,
            arrowwidth=1,
        )
        axis_labels.append(label)

        line = dict(
            visible=True,
            layer='above',
            fillcolor='rgba(0, 0, 0, 0)',
            line=dict(
                color='rgb(125, 125, 125)',
                width=1,
                dash='dot'
            ),
            type='line',
            xref='x',
            x0=0,
            x1=1.7 * np.sin(tick * np.pi / 180.),
            yref='y',
            y0=0,
            y1=1.7 * np.cos(tick * np.pi / 180.),
        )
        axis_lines.append(line)



    for tick in theta_axis_ticks:

        label = dict(
            visible=True,
            clicktoshow=False,
            opacity='1',
            bgcolor='rgba(256, 256, 256, 0.5)',
            # bordercolor='rgba(0, 0, 0, 0)',
            # borderpad='1',
            borderwidth='0',
            text='{}º'.format(tick),
            textangle=0,
            align='center',
            font=dict(
                color='rgba(176, 176, 176)',
            ),
            xref='x',
            xanchor='center',
            x=0,
            ax=30,
            xshift='0',
            yref='y',
            yanchor='middle',
            y=tick * np.pi / 180.,
            ay=0,
            yshift='0',
            showarrow=True,
            arrowhead=0,
            arrowsize=1,
            arrowwidth=1,
            arrowcolor='rgb(176, 176, 176)',
        )
        axis_labels.append(label)

        if tick != 0 or tick != 90:
            line = dict(
                visible=True,
                layer='above',
                fillcolor='rgba(0, 0, 0, 0)',
                line=dict(
                    color='rgb(125, 125, 125)',
                    width=1,
                    dash='dot'
                ),
                type='circle',
                xref='x',
                x0=-tick * np.pi / 180.,
                x1=tick * np.pi / 180.,
                yref='y',
                y0=-tick * np.pi / 180.,
                y1=tick * np.pi / 180.,
            )
            axis_lines.append(line)



    layout = go.Layout(
        title='',
        # width='100%',
        height=800,
        # autosize=False,
        hidesources=True,
        xaxis=dict(
            # visible = False,
            showgrid=False,
            zeroline = False,
            showline=False,
            # autotick=True,
            ticks='',
            showticklabels=False,
            # domain = [0, 0.45],

        ),
        yaxis=dict(
            # visible = False,
            showgrid=False,
            zeroline = False,
            showline=False,
            # autotick=True,
            ticks='',
            showticklabels=False,
            scaleanchor="x",
            scaleratio=1,
            # domain=[0, 0.45],
        ),
        annotations=axis_labels,
        shapes=[
            *axis_lines,
            dict(
                visible=True,
                layer='below',
                fillcolor='rgba(0, 0, 0, 1)',
                line=dict(
                    color='rgb(0, 0, 0)',
                    width=2,
                ),
                type='circle',
                xref='x',
                x0=-1.57,
                x1=1.57,
                yref='y',
                y0=-1.57,
                y1=1.57,
            ),
        ]
    )

    fig = go.Figure(data=data, layout=layout)
    mode_bar_buttons_to_remove = ['sendDataToCloud', 'zoomIn2d', 'zoomOut2d', 'resetScale2d', 'toggleSpikelines',
                                  'hoverClosestCartesian', 'hoverCompareCartesian']

    #

    plot_div = py.plot(fig, show_link=False, output_type='div', include_plotlyjs=False, modeBarButtonsToRemove=mode_bar_buttons_to_remove)


    return plot_div


def generate_angular_surface(x, y, intensity):

    # plot(figure_or_data, show_link=True, link_text='Export to plot.ly', validate=True, output_type='file',
    #      include_plotlyjs=True, filename='temp-plot.html', auto_open=True, image=None, image_filename='plot_image',
    #      image_width=800, image_height=600)

    data = [
        go.Surface(
            x=x,
            y=y,
            z=intensity,
            colorscale='Viridis',
        )
    ]

    layout = go.Layout(
        title='',
        # width='100%',
        height=800,
        hidesources=True,
    )

    fig = go.Figure(data=data, layout=layout)

    plot_div = py.plot(fig, show_link=False, output_type='div', include_plotlyjs=False)

    return plot_div


@measurements.route("/bokeh1")
def bokeh1():
    """ Very simple embedding of a polynomial chart
    """

    from bokeh.embed import components
    from bokeh.plotting import figure
    from bokeh.resources import INLINE, CDN
    from bokeh.util.string import encode_utf8

    colors = {
        'Black': '#000000',
        'Red': '#FF0000',
        'Green': '#00FF00',
        'Blue': '#0000FF',
    }

    def getitem(obj, item, default):
        if item not in obj:
            return default
        else:
            return obj[item]

    # Grab the inputs arguments from the URL
    args = request.args

    # Get all the form arguments in the url with defaults
    color = getitem(args, 'color', 'Black')
    _from = int(getitem(args, '_from', 0))
    to = int(getitem(args, 'to', 10))

    # Create a polynomial line graph with those arguments
    x = list(range(_from, to + 1))
    fig = figure(title="Polynomial")
    fig.line(x, [i ** 2 for i in x], color=colors[color], line_width=2)

    js_resources = CDN.render_js()
    css_resources = CDN.render_css()

    script, div = components(fig)
    html = render_template(
        'embed.html',
        plot_script=script,
        plot_div=div,
        js_resources=js_resources,
        css_resources=css_resources,
        color=color,
        _from=_from,
        to=to
    )
    return encode_utf8(html)


@measurements.route("/bokeh2")
def bokeh2():

    from app import app
    app.logger.info("heheh")

    from jinja2 import Template

    from bokeh.embed import components
    from bokeh.models import Range1d
    from bokeh.plotting import figure
    from bokeh.resources import INLINE, CDN
    # from bokeh.util.browser import view

    # create some data
    x1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12]
    y1 = [0, 8, 2, 4, 6, 9, 5, 6, 25, 28, 4, 7]
    x2 = [2, 5, 7, 15, 18, 19, 25, 28, 9, 10, 4]
    y2 = [2, 4, 6, 9, 15, 18, 0, 8, 2, 25, 28]
    x3 = [0, 1, 0, 8, 2, 4, 6, 9, 7, 8, 9]
    y3 = [0, 8, 4, 6, 9, 15, 18, 19, 19, 25, 28]

    # select the tools we want
    TOOLS = "pan,wheel_zoom,box_zoom,reset,save"

    # the red and blue graphs will share this data range
    xr1 = Range1d(start=0, end=30)
    yr1 = Range1d(start=0, end=30)

    # only the green will use this data range
    xr2 = Range1d(start=0, end=30)
    yr2 = Range1d(start=0, end=30)

    # build our figures
    p1 = figure(x_range=xr1, y_range=yr1, tools=TOOLS, plot_width=600, plot_height=300)
    p1.scatter(x1, y1, size=12, color="red", alpha=0.5)

    p2 = figure(x_range=xr1, y_range=yr1, tools=TOOLS, plot_width=300, plot_height=300)
    p2.scatter(x2, y2, size=12, color="blue", alpha=0.5)

    p3 = figure(x_range=xr2, y_range=yr2, tools=TOOLS, plot_width=300, plot_height=300)
    p3.scatter(x3, y3, size=12, color="green", alpha=0.5)

    # plots can be a single Bokeh model, a list/tuple, or even a dictionary
    plots = {'Red': p1, 'Blue': p2, 'Green': p3}

    script, div = components(plots)

    app.logger.info('{} \n {}'.format(div, script))


    template = Template('''<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>Bokeh Scatter Plots</title>
            {{ js_resources }}
            {{ css_resources }}
            {{ script }}
            <style>
                .embed-wrapper {
                    width: 50%;
                    height: 400px;
                    margin: auto;
                }
            </style>
        </head>
        <body>
            {% for key in div.keys() %}
                <div class="embed-wrapper">
                {{ div[key] }}
                </div>
            {% endfor %}
        </body>
    </html>
    ''')

    js_resources = CDN.render_js()
    css_resources = CDN.render_css()

    # filename = 'embed_multiple.html'

    html = template.render(js_resources=js_resources,
                           css_resources=css_resources,
                           script=script,
                           div=div)

    return html


@measurements.route("/bokeh3")
def bokeh3():

    import io

    from jinja2 import Template

    from bokeh.embed import components
    from bokeh.resources import INLINE
    from bokeh.util.browser import view
    from bokeh.themes import Theme
    from bokeh.plotting import figure

    x1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    y1 = [0, 8, 2, 4, 6, 9, 5, 6, 25, 28, 4]

    p1 = figure(title='DARK THEMED PLOT')
    p1.scatter(x1, y1)

    theme = Theme(json={
        'attrs': {
            'Figure': {
                'background_fill_color': '#2F2F2F',
                'border_fill_color': '#2F2F2F',
                'outline_line_color': '#444444'
                },
            'Axis': {
                'axis_line_color': "white",
                'axis_label_text_color': "white",
                'major_label_text_color': "white",
                'major_tick_line_color': "white",
                'minor_tick_line_color': "white",
                'minor_tick_line_color': "white"
                },
            'Grid': {
                'grid_line_dash': [6, 4],
                'grid_line_alpha': .3
                },
            'Circle': {
                'fill_color': 'lightblue',
                'size': 10,
                },
            'Title': {
                'text_color': "white"
                }
            }
        })

    theme.apply_to_model(p1)

    script, div = components(p1)

    template = Template('''<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <title>Bokeh Scatter Plots</title>
            {{ js_resources }}
            {{ css_resources }}
            {{ script }}
            <style>
                body {
                    background: #2F2F2F;
                }
                .embed-wrapper {
                    width: 50%;
                    height: 400px;
                    margin: auto;
                }
            </style>
        </head>
        <body>
            <div class="embed-wrapper">
            {{ div }}
            </div>
        </body>
    </html>
    ''')

    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()


    html = template.render(js_resources=js_resources,
                           css_resources=css_resources,
                           script=script,
                           div=div)
    return html

