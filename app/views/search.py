from flask import Blueprint, render_template, flash, redirect, url_for, request, g

search = Blueprint('search', __name__, url_prefix='/search')


# @search.before_request
# def before_request():
#     g.search_form = SearchForm()


# Our index-page just shows a quick explanation. Check out the template
# "templates/index.html" documentation for more details.
@search.route('/', methods=['POST'])
def search_input():
    print(request.form)
    print(request.values)

    if request.method == 'POST':
        if not g.search_form.validate_on_submit():
            print('heehe')
            return redirect(url_for('frontend.index'))

    return redirect(url_for('search_results', query=g.search_form.search.data))


@search.route('/search_results/<query>')
def search_results(query):
    # results = Post.query.whoosh_search(query, MAX_SEARCH_RESULTS).all()
    return query