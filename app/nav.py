from flask import current_app, url_for
from app.models.category import Category

from flask_nav import Nav
from flask_nav.elements import NavigationItem, Navbar, View, Subgroup, Separator
from flask_bootstrap.nav import BootstrapRenderer, sha1

from dominate import tags


class ExtendedNav(Nav):

    def __init__(self, app=None):
        self.initializers = []
        super().__init__(app)


    def initializer(self, fn):
        """Adds a initializer function.
        If you want to initialize the navigation bar within a Flask app
        context, you can use this decorator.
        The decorated function should nave one paramater ``nav`` which is the
        bound navigation extension instance.
        """
        self.initializers.append(fn)
        return fn


nav = ExtendedNav()


class ExtendedNavbar(Navbar):
    def __init__(self, title, items, searchbox=None):
        super().__init__(title, *items)
        self.searchbox = searchbox



class SearchBox(NavigationItem):
    def __init__(self, text, endpoint, **kwargs):
        self.text = text
        self.endpoint = endpoint
        self.url_for_kwargs = kwargs

    def get_url(self):
        """Return url for this item.

        :return: A string with a link.
        """
        return url_for(self.endpoint, **self.url_for_kwargs)


@nav.renderer()
class CustomBootstrapRenderer(BootstrapRenderer):

    def visit_SearchBox(self, node):

        search_content = tags.div(cls="col-sm-3 pull-right")

        form = search_content.add(tags.form(name='search', cls='navbar-form', role='search', action=node.get_url(), method='POST'))
        group = form.add(tags.div(cls='input-group'))
        group.add(
            tags.input(
                cls='form-control',
                placeholder=node.text,
                name='q'
            )
        )

        with group.add(tags.div(cls='input-group-btn')):
            with tags.button(cls='btn btn-default'):
                tags.i(cls='glyphicon glyphicon-search')

        return search_content
        

    def visit_Navbar(self, node):
        # create a navbar id that is somewhat fixed, but do not leak any
        # information about memory contents to the outside
        node_id = self.id or sha1(str(id(node)).encode()).hexdigest()

        root = tags.nav() if self.html5 else tags.div(role='navigation')
        root['class'] = 'navbar navbar-default'

        cont = root.add(tags.div(_class='container-fluid'))

        # collapse button
        header = cont.add(tags.div(_class='navbar-header'))
        btn = header.add(tags.button())
        btn['type'] = 'button'
        btn['class'] = 'navbar-toggle collapsed'
        btn['data-toggle'] = 'collapse'
        btn['data-target'] = '#' + node_id
        btn['aria-expanded'] = 'false'
        btn['aria-controls'] = 'navbar'

        btn.add(tags.span('Toggle navigation', _class='sr-only'))
        btn.add(tags.span(_class='icon-bar'))
        btn.add(tags.span(_class='icon-bar'))
        btn.add(tags.span(_class='icon-bar'))

        # title may also have a 'get_url()' method, in which case we render
        # a brand-link
        if node.title is not None:
            if hasattr(node.title, 'get_url'):
                header.add(tags.a(node.title.text, _class='navbar-brand',
                                  href=node.title.get_url()))
            else:
                header.add(tags.span(node.title, _class='navbar-brand'))

        bar = cont.add(tags.div(
            _class='navbar-collapse collapse',
            id=node_id,
        ))
        bar_list = bar.add(tags.ul(_class='nav navbar-nav'))

        for item in node.items:
            bar_list.add(self.visit(item))


        if node.searchbox is not None:
            bar.add(self.visit(node.searchbox))

        return root



@nav.navigation()
def main():

    categories = Category.query.filter(Category.id != 1).all()

    material_list = [View(category.name, 'materials.by_category', category_slug=category.slug, page=1) for category in categories]
    material_nav = Subgroup('Materials', *material_list)

    top_nav = ExtendedNavbar(
        title=View('Spectra App', 'frontend.index'),
        items=(
            View('Home', 'frontend.index'),
            material_nav,
            View('Measurements', 'materials.index'),
            View('Admin', 'admin.index', )
        ),
        searchbox=SearchBox('Search', 'search.search_input')
    )


    if current_app.debug:
        top_nav.items.append(View('Debug-Info', 'debug.debug_root'))

    return top_nav



@nav.initializer
def init_navbar(nav):
    pass


# NOTE: alaternative way
# @navbar.initializer
# def init_navbar(nav):
#     # yield items from database here
#     def yield_items():
#         for i in range(2):
#             external_url = '//example.com/x/%d' % i
#             yield nav.Item('X:%d' % i, endpoint='x-serial', url=external_url)
#
#     # extend your bar
#     nav['top'].extend(yield_items(), after_endpoint='article')


# https://gist.github.com/thedod/eafad9458190755ce943e7aa58355934
#
# class ExtendedNavbar(NavigationItem):
#     def __init__(self, title, root_class='navbar navbar-default', items=[], right_items=[]):
#         self.title = title
#         self.root_class = root_class
#         self.items = items
#         self.right_items = right_items
#
#
# class CustomBootstrapRenderer(BootstrapRenderer):
#
#     def visit_ExtendedNavbar(self, node):
#         # create a navbar id that is somewhat fixed, but do not leak any
#         # information about memory contents to the outside
#         node_id = self.id or sha1(str(id(node)).encode()).hexdigest()
#
#         root = tags.nav() if self.html5 else tags.div(role='navigation')
#         root['class'] = node.root_class
#
#         cont = root.add(tags.div(_class='container-fluid'))
#
#         # collapse button
#         header = cont.add(tags.div(_class='navbar-header'))
#         btn = header.add(tags.button())
#         btn['type'] = 'button'
#         btn['class'] = 'navbar-toggle collapsed'
#         btn['data-toggle'] = 'collapse'
#         btn['data-target'] = '#' + node_id
#         btn['aria-expanded'] = 'false'
#         btn['aria-controls'] = 'navbar'
#
#         btn.add(tags.span('Toggle navigation', _class='sr-only'))
#         btn.add(tags.span(_class='icon-bar'))
#         btn.add(tags.span(_class='icon-bar'))
#         btn.add(tags.span(_class='icon-bar'))
#
#         # title may also have a 'get_url()' method, in which case we render
#         # a brand-link
#         if node.title is not None:
#             if hasattr(node.title, 'get_url'):
#                 header.add(tags.a(node.title.text, _class='navbar-brand',
#                                   href=node.title.get_url()))
#             else:
#                 header.add(tags.span(node.title, _class='navbar-brand'))
#
#         bar = cont.add(tags.div(
#             _class='navbar-collapse collapse',
#             id=node_id,
#         ))
#         bar_list = bar.add(tags.ul(_class='nav navbar-nav'))
#         for item in node.items:
#             bar_list.add(self.visit(item))
#
#         if node.right_items:
#             right_bar_list = bar.add(tags.ul(_class='nav navbar-nav navbar-right'))
#             for item in node.right_items:
#                 right_bar_list.add(self.visit(item))
#
#         return root
#
# def init_custom_nav_renderer(app):
#     # For some reason, this didn't seem to do anything...
#     app.extensions['nav_renderers']['bootstrap'] = (__name__, 'CustomBootstrapRenderer')
#     # ... but this worked. Weird.
#     app.extensions['nav_renderers'][None] = (__name__, 'CustomBootstrapRenderer')
#







# @nav.navigation()
# def main():
#
#     top_nav = Navbar(
#         View('Spectra App', 'frontend.index'),
#         View('Home', 'frontend.index'),
#         View('Materials', 'measurements.measurement'),
#         View('Measurements', 'measurements.measurement'),
#         Subgroup('Measurements',
#                  View('Materials', 'measurements.measurement'),
#                  View('Measurements', 'measurements.measurement'),
#         ),
#         View('Admin', 'admin.index', )
#     )
#
#     if current_app.debug:
#         top_nav.items.append(View('Debug-Info', 'debug.debug_root'))
#
#     return top_nav
#

'''
Notes:
- !! endpoint: name of view function not url!
  hello always finds the apps endpoints "hello"
  foo.hello finds the endpoint named "hello" from blueprint "foo"
  bar.hello finds the endpoint named "hello" from blueprint "bar"
  .hello finds the endpoint "hello" from "foo" if called from a view of "foo", 
  likewise the "hello" from the app or "bar" otherwise.

- To keep things clean, we keep our Flask-Nav instance in here. We will define
  frontend-specific navbars in the respective frontend, but it is also possible
  to put share navigational items in here.

- Custom Renderer for navbar:

    from flask_nav.renderers import Renderer
    from flask_bootstrap.nav import BootstrapRenderer

    @nav.renderer()
    class NavRenderer(BootstrapRenderer):
        def visit_NoneType(self, node):
            return {}

'''

