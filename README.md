# Flask Application for Cathodoluminescence spectra database

## Installation
1. Install Python
2. Install requrements:
```
pip -r requrements.txt
```

## Running
1. Create file `local_config.py`:
```
# Use a secure, unique and absolutely secret key for signing the data.
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"

# Username and password for the Flask-BasicAuth extensions (using for Admin page)
BASIC_AUTH_USERNAME = 'admin'
BASIC_AUTH_PASSWORD = 'secret'

```
2. Run `python run.py` to start Flask at 127.0.0.1:5000 


## Tutorials

